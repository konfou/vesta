## Vesta

Portable source control management system targeted at supporting development of software systems of almost any size.
It was developed at the Compaq/Digital Systems Research Center and written in C++, C and sh.
In the latter half of 2001, Vesta was released by Compaq under the GNU LGPL.

Official site @ [http://www.vestasys.org/]
