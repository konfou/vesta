#! /bin/sh

# Shell script to update a repository to use the new $LAST feature
# for latest links.

# Beginning with /vesta/vestasys.org/repos/23 (bugfix in repos/25), if
# a stub or ghost has a symlink-to attribute whose value is the
# special token $LAST, the NFS layer manifests it as a symlink whose
# value is the arc in the current directory that consists entirely of
# decimal digits, has no extra leading zeroes ("0" itself is OK), is
# not bound to a ghost or stub, and has the largest numeric value of
# all such arcs.  If there are no such arcs, the value is -1.

# Beginning with repos_ui/8 and repltools/12, the tools set latest's
# symlink-to attribute to $LAST upon creation and leave it alone
# thereafter.

# This script updates all the existing latest links in your repository
# to use $LAST.  Run it as vadmin after updating to the new repository
# and tools.

# Determine the repository root from the configuration file
root=`vgetconfig UserInterface AppendableRootName`

# Only use -nloeaf if it looks like we have GNU find
noleaf=""
if find --version 2>&1 | grep -q "GNU"; then
  noleaf="-noleaf"
fi

find $root $noleaf -type l -name latest \
  -exec vattrib -s symlink-to '$LAST' {} \;
